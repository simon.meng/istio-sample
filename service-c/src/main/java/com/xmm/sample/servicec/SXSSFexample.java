package com.xmm.sample.servicec;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class SXSSFexample {


    public static void main(String[] args) throws Throwable {
        int loop =0;
        do {
            //FileInputStream inputStream = new FileInputStream("tempsxssf.xlsx");
            XSSFWorkbook wb_template = new XSSFWorkbook(new File("tempsxssf.xlsx"));
            //inputStream.close();
            wb_template.close();

            SXSSFWorkbook wb = new SXSSFWorkbook(wb_template);
            wb.setCompressTempFiles(true);
            SXSSFSheet sh = wb.createSheet();

            sh.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
            int count = sh.getLastRowNum();
            System.out.println("count = " + count + " loop = " + loop);
            for (int rownum = count; rownum <  count + 500; rownum++) {
                Row row = sh.createRow(rownum);
                for (int cellnum = 0; cellnum < 10; cellnum++) {
                    Cell cell = row.createCell(cellnum);
                    String address = new CellReference(cell).formatAsString();
                    cell.setCellValue(address);
                }

            }
            FileOutputStream out = new FileOutputStream("tempsxssf.xlsx");
            wb.write(out);
            out.close();
        } while (loop++ < 1000);
    }

}