package com.xmm.sample.servicec;

import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by simon on 20/07/2018.
 */
public class Transformer {

    public static String REGEX_WORD = "[0-9a-zA-Z\\-]+";
    public static String REGEX_WORD_GROUP = "([0-9a-zA-Z\\-]+)";

    public static String REGEX_SQUENCE = "[东南西北甲乙丙丁]{1,2}";
    public static String REGEX_SQUENCE_GROUP = "([东南西北甲乙丙丁]{1,2})";

    public static class ConverterRule {
        List<Rule> rules = new ArrayList<>();
        Map<String, String> mapping = new HashMap<>();

        public ConverterRule(String src, String dest) {
            add(src, dest);
        }

        public ConverterRule add(String src, String dest) {
            Rule rule = new Rule(src, dest);
            rules.add(rule);
            mapping.putAll(rule.getMapping());
            return this;
        }

        public List<Rule> getRules() {
            return rules;
        }

        public Map<String, String> getMapping() {
            return mapping;
        }

        public static class Rule {
            LinkedList<DiffMatcher.Diff> diffList;
            Map<String, String> mapping = new HashMap<>();
            boolean isAlphanumericChanged;

            public Rule(String src, String dest) {
                diffList = new DiffMatcher().diff_main(src, dest);
                diffList.forEach(diff -> {
                    if (diff.operation == DiffMatcher.Operation.DELETE) {
                        if (diff.text.matches("\\w")) {
                            DiffMatcher.Diff next = nextDiff(diffList, diff);
                            if (next != null && next.operation == DiffMatcher.Operation.INSERT) {
                                mapping.putIfAbsent(diff.text, next.text);
                            }
                        }
                    }
                });
                this.isAlphanumericChanged = checkAlphanumericChanged(src, dest);
            }

            public boolean checkAlphanumericChanged(String src, String dest) {
                List<String> list1 = StringsUtils.extractAlphanumeric(src);
                List<String> list2 = StringsUtils.extractAlphanumeric(src);
                if (list1.size() == list2.size()) {
                    for(int i =0;i<list1.size();i++) {
                        if (!list1.get(i).equals(list2.get(i))) {
                            return false;
                        }
                    }
                    return true;
                }

                return false;
            }

            public boolean isAlphanumericChanged() {
                return isAlphanumericChanged;
            }

            public LinkedList<DiffMatcher.Diff> getDiffList() {
                return diffList;
            }

            public Map<String, String> getMapping() {
                return mapping;
            }
        }
    }

    public static class UnableToTransformException extends Exception {

    }


    public static String transform(ConverterRule.Rule rule, String c) throws UnableToTransformException {
        final StringBuilder stringBuilder = new StringBuilder(c);
        rule.diffList.forEach(new Consumer<DiffMatcher.Diff>() {
            int index = 0;
            String lastReplaceStr = "";

            @Override
            public void accept(DiffMatcher.Diff diff) {
                if (diff.operation == DiffMatcher.Operation.INSERT) {
                    DiffMatcher.Diff prev = prevDiff(rule.diffList, diff);
                    if (prev != null && prev.operation == DiffMatcher.Operation.DELETE && prev.text.matches("[a-zA-Z]+")) {
                        String insert = rule.getMapping().get(lastReplaceStr);
                        if (insert != null) {
                            stringBuilder.insert(index, insert);
                            index = index + insert.length();
                        } else {
                            //throw new RuntimeException("Not Matched!");
                            //TODO: throw exception
                            stringBuilder.insert(index, diff.text);
                            index = index + diff.text.length();
                        }
                    } else {
                        stringBuilder.insert(index, diff.text);
                        index = index + diff.text.length();
                    }
                } else if (diff.operation == DiffMatcher.Operation.EQUAL) {
                    String exp = toMatchGroupRegx(diff.text);
                    Pattern p = Pattern.compile(exp);
                    if (stringBuilder.length() > index) {
                        Matcher matcher = p.matcher(stringBuilder.toString());
                        if (matcher.find(index)) {
                            if (matcher.group().length() > 0) {
                                String theGroup = matcher.group(1);
                                index = index + theGroup.length();
                            }
                        } else {
                            //TODO: throws exception
                        }
                    }
                } else {
                    String exp = toMatchGroupRegx(diff.text);
                    Pattern p = Pattern.compile(exp);
                    if (stringBuilder.length() > index) {
                        Matcher matcher = p.matcher(stringBuilder.toString());
                        if (matcher.find(index)) {
                            if (matcher.group().length() > 0) {
                                String theGroup = matcher.group(1);
                                lastReplaceStr = theGroup;
                                stringBuilder.replace(index, index + theGroup.length(), "");
                            }
                        }
                    }

                }
            }
        });
        return stringBuilder.toString();
    }

    public static DiffMatcher.Diff nextDiff(LinkedList<DiffMatcher.Diff> diffLinkedList, DiffMatcher.Diff diff) {
        int current = diffLinkedList.indexOf(diff);
        if (current < diffLinkedList.size() - 1) {
            return diffLinkedList.get(current + 1);
        }
        return null;
    }

    public static DiffMatcher.Diff nextEqualOrInsertDiff(LinkedList<DiffMatcher.Diff> diffLinkedList, DiffMatcher.Diff diff) {
        int current = diffLinkedList.indexOf(diff);
        if (current < diffLinkedList.size() - 1) {
            return diffLinkedList.get(current + 1);
        }
        return null;
    }

    public static DiffMatcher.Diff prevDiff(LinkedList<DiffMatcher.Diff> diffLinkedList, DiffMatcher.Diff diff) {
        int current = diffLinkedList.indexOf(diff);
        if (current >= 1) {
            return diffLinkedList.get(current - 1);
        }
        return null;
    }

    public static String toMatchGroupRegx(String str) {
        return "("+str.replaceAll("[0-9a-zA-Z\\-]+","[0-9a-zA-Z\\-]+").replaceAll("[东南西北甲乙丙丁]{1,2}","[东南西北甲乙丙丁]{1,2}")+")";
    }
}
