package com.xmm.sample.servicec;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by simon on 24/07/2018.
 */
public class StringsUtils {
    public static List<String> extractAlphanumeric(String str) {
        List<String> list = new ArrayList<>();
        Pattern pattern = Pattern.compile("([0-9a-zA-Z\\-]+)");
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()) {
            String group = matcher.group(1);
            System.out.println(matcher.start(1) + " -- " + matcher.end(1));
            list.add(group);
        }
        return list;
    }

    public static List<String> toCharAndAlphanumerics(String str) {
        List<String> list = new ArrayList<>();
        Pattern pattern = Pattern.compile("([0-9a-zA-Z\\-]+)");
        Matcher matcher = pattern.matcher(str);
        boolean found = false;
        int lastGroupIndex = 0;
        while (matcher.find()) {
            found = true;
            String group = matcher.group(1);
            char[] chars = str.substring(lastGroupIndex, matcher.start(1)).toCharArray();
            for (char c : chars) {
                list.add(String.valueOf(c));
            }
            lastGroupIndex = matcher.end(1);
            list.add(group);
        }

        if (found) {
            if (lastGroupIndex != str.length()) {
                char[] chars = str.substring(lastGroupIndex).toCharArray();
                for (char c : chars) {
                    list.add(String.valueOf(c));
                }
            }
        } else {
            char[] chars = str.toCharArray();
            for (char c : chars) {
                list.add(String.valueOf(c));
            }
        }
        return list;
    }


    @Test
    public void test() {
        System.out.println(toCharAndAlphanumerics("丰乐路88号大院A10栋203"));
        System.out.println(toCharAndAlphanumerics("天河丰乐路88号大院12号A-203房"));

        List<String> a = toCharAndAlphanumerics("丰乐路88号大院A10栋203");
        List<String> b = toCharAndAlphanumerics("天河丰乐路88号大院12号203房");

        System.out.println(new DiffMatcher().diff_main("丰乐路88号大院0A10栋203", "天河丰乐路88号大院012号203房"));
    }

    public void test1() {
        System.out.println(toCharAndAlphanumerics("丰乐路88号大院A10栋203"));
        System.out.println(toCharAndAlphanumerics("天河丰乐路88号大院12号203房"));

        List<String> a = toCharAndAlphanumerics("丰乐路88号大院A10栋203");
        List<String> b = toCharAndAlphanumerics("天河丰乐路88号大院12号203房");

        String str1 = "丰乐路88号大院0A10栋203";
        String str1__ = "天河丰乐路88号大院012号203房";

        LinkedList<DiffMatcher.Diff> diffs = new DiffMatcher().diff_main("丰乐路88号大院0A10栋203", "天河丰乐路88号大院012号203房");

        for (DiffMatcher.Diff diff : diffs) {
            if (diff.operation == DiffMatcher.Operation.DELETE) {
                if (diff.text.matches("[0-9a-zA-Z\\-]+")) {
                    DiffMatcher.Diff prev = Transformer.prevDiff(diffs, diff);
                    DiffMatcher.Diff next = Transformer.nextDiff(diffs, diff);
                    if (prev != null) {
                        if (prev.text.matches(".*[0-9a-zA-Z\\-]+$")) {
                            List<String> numeracy = toCharAndAlphanumerics(prev.text);
                            String numeric = numeracy.get(numeracy.size() - 1);

                            // update prev node
                            prev.text = prev.text.substring(0, prev.text.length() - numeric.length());

                            // update current
                            diff.text = numeric + diff.text;

                            // append the numberic to INSERT/EQUAL diff
                            int cur = diffs.indexOf(diff);
                            while (++cur < diffs.size() - 1) {
                                if (diffs.get(cur).operation == DiffMatcher.Operation.INSERT ||
                                        diffs.get(cur).operation == DiffMatcher.Operation.EQUAL) {
                                    diffs.get(cur).text = numeric + diffs.get(cur).text;
                                    break;
                                }
                            }
                        }
                    }

                    if (next != null) {

                    }

                }


            }


        }
    }

    @Test
    public void test3() {
        System.out.println(new DiffMatcher().diff_main("丰乐路88号大院0A10栋20A", "丰乐路88号大院0A10栋20"));
    }

}
