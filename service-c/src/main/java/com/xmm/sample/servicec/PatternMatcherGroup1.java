package com.xmm.sample.servicec;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternMatcherGroup1 {
    public static void main(String[] args) {
        String stringToSearch = "广州市天河区珠江321号利通H1-223东南广场A座1902宝...";

        String abc = stringToSearch.replaceAll("[0-9a-zA-Z\\-]+","{}");
        System.out.println(abc);

        String regx = "利通H1-223东南广场".replaceAll("[0-9a-zA-Z\\-]+","[0-9a-zA-Z\\-]+").replaceAll("[东南西北]{1,2}","[东南西北]{1,2}");

        System.out.println(regx);

        Pattern p = Pattern.compile(Transformer.toMatchGroupRegx("利通H1-223东南广场"));

        Matcher m = p.matcher(stringToSearch);

        // if we find a match, get the group
        while (m.find()) {
            // we're only looking for one group, so get it
            String theGroup = m.group(1);

            // print the group out for verification
            System.out.format("%s 00000 \n", theGroup);
        }

    }
}