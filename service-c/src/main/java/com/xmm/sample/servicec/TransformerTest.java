package com.xmm.sample.servicec;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.xmm.sample.servicec.StringsUtils.extractAlphanumeric;

/**
 * Created by simon on 22/07/2018.
 */
public class TransformerTest {

    @Test
    public void test1() throws Transformer.UnableToTransformException {
        String a = "广州市天河A区珠江32号利通广场A座1903";
        String b = "广东省广州市天河A区珠江东路32号利通广场101座1903座";
        String c = "广州市天河B区珠江321号利通广场C座1902";
        String d = Transformer.transform(new Transformer.ConverterRule.Rule(a, b), c);
        Assert.assertEquals("广东省广州市天河B区珠江东路321号利通广场101座1902座", d);
    }


    @Test
    public void test2() throws Transformer.UnableToTransformException {
        String a = "广州市天河A区珠江32号利通广场A1-223西南1903";
        String b = "广东省广州市天河A区珠江东路32号利通广场1903房";
        String c = "广州市天河B区珠江1032号利通广场A1-223东南1902";
        String d = Transformer.transform(new Transformer.ConverterRule.Rule(a, b), c);
        Assert.assertEquals("广东省广州市天河B区珠江东路1032号利通广场1902房", d);
    }

    @Test
    public void test3() throws Transformer.UnableToTransformException {
        String a = "广州市天河A区珠江32号利通广场A1-223西南1903";
        String b = "广东省广州市天河A区珠江东路32号利通广场西南1903房";
        String c = "广州市天河B区珠江1032号利通广场A1-223东南1902";
        String d = Transformer.transform(new Transformer.ConverterRule.Rule(a, b), c);
        Assert.assertEquals("广东省广州市天河B区珠江东路1032号利通广场东南1902房", d);
    }


    @Test
    public void testRegex() {
        Assert.assertTrue("123abc".matches("^\\w+.*"));
        Assert.assertTrue("abc123".matches(".*\\d$"));
    }

    @Test
    public void test4ADD_DELELTE() {
        String str1 = "广场123西南";
        String str1__ = "东广场210124西南";
        System.out.println(new DiffMatcher().diff_main(str1, str1__));

    }

    @Test
    public void test4ADD_EQUAL() {

    }

    @Test
    public void test4DELETE_ADD() {
        String str1 = "广场6a西南";
        String str1__ = "东广场6001西南";
        System.out.println(new DiffMatcher().diff_main(str1, str1__));
    }

    @Test
    public void test4DELETE_EQUAL() {

    }


    @Test
    public void test4EQUAL_ADD() {
        System.out.println(RandomStringUtils.randomAlphabetic(10));
        System.out.println(RandomStringUtils.random(10));
        System.out.println(RandomStringUtils.randomAscii(10));
        System.out.println(RandomStringUtils.randomGraph(10));
        System.out.println(RandomStringUtils.randomPrint(10));
        System.out.println(RandomStringUtils.randomAlphabetic(10));
        System.out.println(RandomStringUtils.randomAlphanumeric(6));
    }

    @Test
    public void test4EQUAL_DELETE() {

    }

    @Test
    public void testRegularExpression() {
        String stringToSearch = "利通H1-223西南广场";
        Matcher matcher = Pattern.compile(Transformer.toMatchGroupRegx("利通H1-223东南广场")).matcher(stringToSearch);
        matcher.find();
        Assert.assertEquals(1, matcher.groupCount());
        Assert.assertEquals("利通H1-223西南广场", matcher.group(1));
    }


    @Test
    public void testPairRules1() {
        String str1 = "广场6a西南";
        String str1__ = "东广场601西南";


        String str2 = "广场6b西南";
        String str2__ = "东广场602西南";


        System.out.println(new DiffMatcher().diff_main(str1, str1__));
        System.out.println(new DiffMatcher().diff_main(str1__, str1));


        System.out.println(new DiffMatcher().diff_main(str2, str2__));
        System.out.println(new DiffMatcher().diff_main(str2__, str2));

    }

    @Test
    public void testPairRules2() {
        String str1 = "广场a2西南";
        String str1__ = "东广场102西南";

        String str2 = "广场b2西南";
        String str2__ = "东广场202西南";

        System.out.println(new DiffMatcher().diff_main(str1, str1__));
        System.out.println(new DiffMatcher().diff_main(str1__, str1));


        System.out.println(new DiffMatcher().diff_main(str1, str1__));
        System.out.println(new DiffMatcher().diff_main(str2__, str2));

    }

    @Test
    public void testReplace() {
        String str1 = "丰乐路88号大院A10栋203";
        String str1__ = "丰乐路88号大院12号203房";

        String str2 = "丰乐路88号大院A10栋204";
        String str2__ = "丰乐路88号大院13号204房";

        //System.out.println(new DiffMatcher().diff_main(str1, str1__));
        //System.out.println(new DiffMatcher().diff_main(str2, str2__));

        List list1 = extractAlphanumeric(str1);
        List list1__ = extractAlphanumeric(str1__);

        //List list2 = extractAlphanumeric(str2);
        //List list2__ = extractAlphanumeric(str2__);

        System.out.println(list1);
        System.out.println(list1__);
        //System.out.println(list2);
        //System.out.println(list2__);
    }

    public interface Converter {
        String convert(String str);
    }


    public static class XConverter implements Converter {

        private List<Rule> rules = new ArrayList<>();

        public void addRule(String org, String target) {
            rules.add(new Rule(org, target));
        }

        public String convert(String str) {
            if (rules.isEmpty()) {
                return null;
            }
            if (rules.size() == 0) {
                return rules.get(0).transform(str);
            }
            return null;
        }

        public Map<String, String> overlay(Rule rule1, Rule rule2) {
            return null;
        }
    }

    public interface Strategy {

    }

    public static class Rule {
        private String format;
        private List<String> originalHolders;
        private List<String> targetHolders;
        public Map<Integer, Integer> map = new HashMap<>();

        public boolean isKeywordAllMatched() {
            return originalHolders.containsAll(targetHolders);
        }

        public List<String> getOriginalHolders() {
            return originalHolders;
        }

        public List<String> getTargetHolders() {
            return targetHolders;
        }

        public Rule(String original, String target) {
            this.format = target.replaceAll("[0-9a-zA-Z\\-]+", "%s");
            this.originalHolders = extractAlphanumeric(original);
            this.targetHolders = extractAlphanumeric(original);
            for (int i = 0; i < targetHolders.size(); i++) {
                for (int j = 0; i < originalHolders.size(); j++) {
                    if (targetHolders.get(i).equals(originalHolders.get(j))) {
                        map.put(i, j);
                        break;
                    }
                }
            }
        }

        public String transform(String str) {
            List<String> list = extractAlphanumeric(str);
            if (list.size() == originalHolders.size()) {
                String[] holders = originalHolders.toArray(new String[originalHolders.size()]);
                String[] clone = Arrays.copyOf(holders, holders.length);
                map.entrySet().forEach(e -> clone[e.getKey()] = list.get(e.getValue()));
                return String.format(format, clone);
            }
            return null;
        }
    }


    @Test
    public void testTokens() {
        String str = new Rule("丰乐路88号大院12栋203", "丰乐路88号大院12号203房").transform("丰乐路88号大院A10栋204");
        System.out.println(str);
    }
}