package com.xmm.sample.servicec;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Created by simon on 05/09/2018.
 */
@Component
public class HeaderFilter extends OncePerRequestFilter {



    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        HttpServletRequest httpRequest = request;
        Enumeration<String> headerNames = httpRequest.getHeaderNames();

        if (headerNames != null) {
            while (headerNames.hasMoreElements()) {
                System.out.println("Header: " + httpRequest.getHeader(headerNames.nextElement()));
            }
        }
        System.out.println("request.getLocalAddr()" + request.getLocalAddr());
        System.out.println("request.getRequestURL() " + request.getRequestURL());
        System.out.println("request.getRemoteHost()" + request.getRemoteHost());
        System.out.println("request.getRemoteAddr()" + request.getRemoteAddr());
        //doFilter
        filterChain.doFilter(httpRequest, response);
    }
}
