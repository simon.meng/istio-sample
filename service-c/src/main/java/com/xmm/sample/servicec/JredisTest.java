package com.xmm.sample.servicec;

import org.junit.Test;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;

/**
 * Created by simon on 10/08/2018.
 */
public class JredisTest {

    @Test
    public void test() {
        JedisCluster jedisCluster = new JedisCluster(new HostAndPort("123.127.2.212",9999));

        System.out.println(jedisCluster.ping());
        Jedis jedis = new Jedis("123.127.2.212",9999);
        System.out.println("Connection to server sucessfully");
        //check whether server is running or not
        System.out.println("Server is running: "+jedis.ping());
    }
}
