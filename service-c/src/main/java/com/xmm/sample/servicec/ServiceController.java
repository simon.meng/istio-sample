package com.xmm.sample.servicec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by simon (simon.meng@fox.mal.com) on 17/04/2018.
 */
@RestController
public class ServiceController implements ApplicationContextAware{

    private static final Logger log = LoggerFactory.getLogger(ServiceController.class);

    @Value("${app.version}")
    private String version;

    @Value("${app.version2}")
    private String version2;

    @GetMapping("/info")
    public String info() {
        String version = "C Service version = " + this.version;
        log.info(version);
        return version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion2() {
        return version2;
    }

    public void setVersion2(String version2) {
        this.version2 = version2;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("------version---------");
        System.out.println(version);
        System.out.println("------version2---------");
        System.out.println(version2);
    }
}
