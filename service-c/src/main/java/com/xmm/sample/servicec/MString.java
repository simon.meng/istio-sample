package com.xmm.sample.servicec;

import java.util.Arrays;
import java.util.List;

/**
 * Created by simon on 25/07/2018.
 */
public class MString {

    private String[] value;

    public int length() {
        return value.length;
    }

    public MString(String original) {
        List<String> list = StringsUtils.toCharAndAlphanumerics(original);
        value = StringsUtils.toCharAndAlphanumerics(original).toArray(new String[list.size()]);
    }

    public MString substring(int beginIndex, int endIndex) {
        if (beginIndex < 0) {
            throw new StringIndexOutOfBoundsException(beginIndex);
        }
        if (endIndex > value.length) {
            throw new StringIndexOutOfBoundsException(endIndex);
        }
        int subLen = endIndex - beginIndex;
        if (subLen < 0) {
            throw new StringIndexOutOfBoundsException(subLen);
        }
        return ((beginIndex == 0) && (endIndex == value.length)) ? this
                : new MString(value, beginIndex, subLen);
    }

    public MString substring(int beginIndex) {
        return substring(beginIndex, length());
    }


    public MString(String value[], int offset, int count) {
        if (offset < 0) {
            throw new StringIndexOutOfBoundsException(offset);
        }
        if (count <= 0) {
            if (count < 0) {
                throw new StringIndexOutOfBoundsException(count);
            }
            if (offset <= value.length) {
                List<String> list = StringsUtils.toCharAndAlphanumerics("");
                this.value = list.toArray(new String[list.size()]);
                return;
            }
        }
        // Note: offset or count might be near -1>>>1.
        if (offset > value.length - count) {
            throw new StringIndexOutOfBoundsException(offset + count);
        }
        this.value = Arrays.copyOfRange(value, offset, offset + count);
    }


    public String charAt(int index) {
        if ((index < 0) || (index >= value.length)) {
            throw new StringIndexOutOfBoundsException(index);
        }
        return value[index];
    }


    @Override
    public boolean equals(Object anObject) {
        if (this == anObject) {
            return true;
        }
        if (anObject instanceof MString) {
            MString anotherString = (MString) anObject;
            int n = value.length;
            if (n == anotherString.value.length) {
                String v1[] = value;
                String v2[] = anotherString.value;
                int i = 0;
                while (n-- != 0) {
                    if (!v1[i].equals(v2[i]))
                        return false;
                    i++;
                }
                return true;
            }
        }
        return false;
    }
}
