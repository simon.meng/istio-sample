package com.xmm.sample.portal;

import com.jsoniter.output.JsonStream;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import static org.bouncycastle.cms.RecipientId.password;

/**
 * Created by simon (simon.meng@fox.mal.com) on 17/04/2018.
 */
@RestController
public class ServiceController {

    private static final Logger log = LoggerFactory.getLogger(ServiceController.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${service.a.url}")
    private String aSrvUrl;

    @Value("${service.c.url}")
    private String cSrvUrl;

    @Value("${app.version}")
    private String version;

    @GetMapping("/info")
    public String info() {

        String srvArsp = callAService();
        String srvCrsp = callCService();

        log.info(srvArsp);
        log.info(srvCrsp);

        return String.format("<html>" +
                "<h3>Portal version=" + this.version + "</h3><br/>" +
                "<h3>%s</h3><br/>" +
                "<h3>%s</h3><br/>" +
                "</html>", srvArsp, srvCrsp);
    }


    public String callAService() {
        try {
            return restTemplate.getForObject(aSrvUrl, String.class);
        } catch (RestClientException e) {
            return e.getMessage();
        }
    }


    public String callCService() {
        try {
            return restTemplate.getForObject(cSrvUrl, String.class);
        } catch (RestClientException e) {
            return e.getMessage();
        }
    }


//    public String loadEsdata() {
//        StringBuilder sb = new StringBuilder();
//        for (int i = 1000000; i < 5000000; i++) {
//            Map<String, Map<String, String>> action = new HashMap<>();
//            Map<String, String> doc = new HashMap<>();
//            doc.put("_type", "_doc");
//            doc.put("_id", String.valueOf(i));
//            action.put("create", doc);
//            sb.append(JsonStream.serialize(action)).append("\n");
//
//            Map<String, String> row = new HashMap<>();
//            row.put("street", "tianhe" + i);
//            row.put("user_id", String.valueOf(i));
//            sb.append(JsonStream.serialize(action)).append("\n");
//            sb.append(JsonStream.serialize(row)).append("\n");
//            if (i % 1000 == 0) {
//                long start = System.currentTimeMillis();
//                HttpHeaders headers = new HttpHeaders();
//                headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
//                HttpEntity<String> entity = new HttpEntity<>(sb.toString(), headers);
//                Object o = new RestTemplate().postForObject("http://192.168.99.20:30986/address/_bulk", entity, Object.class);
//                System.out.println(System.currentTimeMillis() - start);
//                sb = new StringBuilder();
//            }
//        }
//        return "done";
//    }

    public static void loadCSV() throws IOException {
        Reader in = new FileReader("/Users/simon/Desktop/address.csv");
        Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(in);
        int cnt = 0;
        StringBuilder sb = new StringBuilder();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        RestTemplate restTemplate = new RestTemplate();

        H h = new H(4, o1 -> restTemplate.postForObject("http://192.168.99.20:30986/addresses2/_bulk", new HttpEntity<>(o1, headers), Object.class));
        for (CSVRecord record : records) {
            Map<String, Map<String, String>> action = new HashMap<>();
            Map<String, String> doc = new HashMap<>();
            doc.put("_type", "address");
            doc.put("_id", String.valueOf(cnt));
            action.put("create", doc);
            sb.append(JsonStream.serialize(action)).append("\n");
            Map<String, String> row = new HashMap<>();
            row.put("distinct", record.get(0));
            row.put("country", record.get(1));
            row.put("community", record.get(2));
            row.put("streetName", record.get(3));
            row.put("addressName", record.get(4));
            row.put("addressNum1", record.get(5));
            row.put("addressNum2", record.get(6));
            row.put("fullName", record.get(7));
            row.put("roomNum", record.get(8));
            row.put("addressNum3", record.get(9));
            row.put("addressDetail", record.get(10));
            row.put("homeAddress", record.get(11));
            sb.append(JsonStream.serialize(row)).append("\n");
            if (cnt++ % 1000 == 0) {
                h.put(sb.toString());
                sb = new StringBuilder();
            }

        }
    }

    interface Handler<T> {
        void handle(T t);
    }


    public static class H<T> {
        private LinkedBlockingQueue<T> queue = new LinkedBlockingQueue<T>(10);
        private Handler handler;

        public H(int size, Handler<T> handler) {
            this.handler = handler;
            ExecutorService executorService = Executors.newFixedThreadPool(size);
            for (int i = 0; i < size; i++) {
                executorService.execute(() -> process());
            }
        }

        public void put(T t) {
            try {
                queue.put(t);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void process() {
            while (true) {
                try {
                    T t = queue.take();
                    long start = System.currentTimeMillis();
                    handler.handle(t);
                    System.out.println(System.currentTimeMillis() - start);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }

    }


    public static void main(String[] args) throws IOException {
        //new ServiceController().loadEsdata();
        loadCSV();
    }
}
