package com.xmm.sample.servicea;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.parser.PdfImageObject;
import org.apache.commons.io.FilenameUtils;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by simon on 07/07/2018.
 */
public class PdfUtils {


    public static void toSameSize(String pdf) throws Exception {
        pdf2image(pdf);
        String pdfdir = pdf.substring(0, pdf.length() - 4);
        convertImage2Pdf(pdf + "_same_size.pdf",
                file -> file.length() > 3,
                pdfdir);
    }

    public static void toSameSizeFromDir(String dir) throws Exception {
        Files.find(Paths.get(dir), 1, (path, basicFileAttributes) -> Files.isRegularFile(path)).forEachOrdered(path -> {
            try {
                if (!path.toFile().getPath().endsWith("_same_size.pdf")) {
                    toSameSize(path.toFile().getPath());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    public static void main(String[] args) throws Exception {

        toSameSize("/Users/simon/Downloads/免班主任证明和育人材料.pdf");
//        toSameSizeFromDir("/Users/simon/tools/demo/公开课大小不一样");


//        pdf2image("/Users/simon/Downloads/2017学年还要增加2018年度考核.pdf");
//                compressPdf(0.85f, "/Users/simon/Downloads/a1111.pdf",
//                "/Users/simon/Downloads/2017学年还要增加2018年度考核-compressed2.pdf");


//        convertImage2Pdf(
//                "/Users/simon/src/istio-sample.git/service-a/src/main/k8s/2015/small.pdf",
//                file ->  file.length() / (1024 * 1024) <= 3 && !file.getName().startsWith("."),
//                "/Volumes/MDISK/summer/2015学年");
//
//
 //       compressPdf(0.65f, "/Users/simon/tools/demo/已变要压缩到10M以下.pdf",
//                "/Users/simon/tools/demo/已变要压缩到10M以下-compressed.pdf");

//
//        mergePdf("/Users/simon/src/istio-sample.git/service-a/src/main/k8s/large-small-combined.pdf",
//                "/Users/simon/src/istio-sample.git/service-a/src/main/k8s/2015/large-compressed.pdf",
//                "/Users/simon/src/istio-sample.git/service-a/src/main/k8s/2015/small.pdf");


        //compressPdf(0.8f,"/Users/simon/Downloads/育人材料，培养青年教师，学生竞赛辅导计划和考勤表.PDF","/Users/simon/Downloads/育人材料，培养青年教师，学生竞赛辅导计划和考勤表.PDF-compressed.pdf");

        //convertImage2Pdf( "/Users/simon/Desktop/abc/CDF888235CD037071537D2588FC55FAC.jpg","/Users/simon/Desktop/abc/1.pdf");
    }


    public static void pdf2image(String file) throws Exception {
        PDDocument document = PDDocument.load(new File(file));
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        File dir = new File(file.substring(0, file.length() - 4));
        dir.mkdirs();
        for (int page = 0; page < document.getNumberOfPages(); ++page) {
            PDPage pdPage = document.getPage(page);
            System.out.println(pdPage.getBBox().getWidth());
            float dpi = 300f;
            dpi = (595.2f / pdPage.getBBox().getWidth()) * dpi;
            BufferedImage image = pdfRenderer.renderImageWithDPI(page, dpi, ImageType.RGB);

            String imgFile = dir.getPath() + File.separator + page + ".jpg";
            ImageOutputStream ios = new FileImageOutputStream(new File(imgFile));

            ImageIO.write(image, "JPG", ios);
            ios.close();

//            //file size > 1MB compress image
//            if(new File(imgFile).length() / (1024 * 1024) > 1){
//
//                image = ImageIO.read(new File(imgFile));
//                ios = new FileImageOutputStream(new File(imgFile +".compress.jpg"));
//
//                Iterator<ImageWriter> writers =  ImageIO.getImageWritersByFormatName("jpg");
//                ImageWriter writer = writers.next();
//                ImageWriteParam param = writer.getDefaultWriteParam();
//
//                writer.setOutput(ios);
//
//                param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
//                param.setCompressionQuality((1.0f/(new File(file).length()/(1024*1024f))));
//                writer.write(null, new IIOImage(image, null, null), param);
//                ios.close();
//                writer.dispose();
//            }
        }
        document.close();
    }


    public static void mergePdf(String pdfPath, String... inputDirsAndFiles) throws Exception {
        PDFMergerUtility pdfMergerUtility = new PDFMergerUtility();
        pdfMergerUtility.setDestinationFileName(pdfPath);
        for (String input : inputDirsAndFiles) {
            if (new File(input).isDirectory()) {
                String[] pdfs = new File(input).list();
                Arrays.sort(pdfs);
                for (String pdf : pdfs) {
                    if (!new File(pdf).getName().startsWith(".") && !new File(pdf).getName().equals("merged.pdf")) {
                        System.out.println(input + "/" + pdf);
                        pdfMergerUtility.addSource(input + "/" + pdf);
                    }
                }
            } else {
                System.out.println(input);
                pdfMergerUtility.addSource(input);
            }
        }
        pdfMergerUtility.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());
    }


    public static void convertImage2Pdf(String imagePath, String pdfPath) throws IOException {
        try (PDDocument doc = new PDDocument()) {
            addImageAsNewPage(doc, imagePath);
            doc.save(pdfPath);
        }
    }


    public static void convertImage2Pdf(String pdfPath, FileFilter fileFilter, String... inputDirsAndFiles) throws IOException {
        try (PDDocument doc = new PDDocument()) {
            for (String input : inputDirsAndFiles) {
                if (new File(input).isDirectory()) {

                    List<File> files = Files.find(Paths.get(input),
                            Integer.MAX_VALUE,
                            (path, basicFileAttributes) -> Files.isRegularFile(path)).map(path -> path.toFile()).collect(Collectors.toCollection(ArrayList::new));

                    files.sort(Comparator.comparing(filePath -> new Integer(filePath.getName().substring(0, filePath.getName().length() - 4))));

                    for (File file : files) {
                        if (fileFilter.accept(file)) {
                            addImageAsNewPage(doc, file.getPath());
                        }
                    }

                } else {
                    if (fileFilter.accept(new File(input))) {
                        addImageAsNewPage(doc, input);
                    }
                }
            }
            doc.save(pdfPath);
        }
    }

    private static void addImageAsNewPage(PDDocument doc, String imagePath) {
        try {

            PDImageXObject image = PDImageXObject.createFromFile(imagePath, doc);
            PDRectangle pageSize = PDRectangle.A4;

            int originalWidth = image.getWidth();
            int originalHeight = image.getHeight();

            if (originalWidth > originalHeight) {
                String rotateImgPath = imagePath + UUID.randomUUID() + FilenameUtils.getExtension(imagePath);
                rotate90(imagePath, rotateImgPath);
                image = PDImageXObject.createFromFile(rotateImgPath, doc);
                new File(rotateImgPath).delete();
                originalWidth = image.getWidth();
                originalHeight = image.getHeight();
            }

            float pageWidth = pageSize.getWidth();
            float pageHeight = pageSize.getHeight();

            float ratio = Math.min(pageWidth / originalWidth, pageHeight / originalHeight);

            float scaledWidth = originalWidth * ratio;
            float scaledHeight = originalHeight * ratio;
            float x = (pageWidth - scaledWidth) / 2;
            float y = (pageHeight - scaledHeight) / 2;

            PDPage page = new PDPage(pageSize);
            doc.addPage(page);
            try (PDPageContentStream contents = new PDPageContentStream(doc, page)) {
                contents.drawImage(image, x, y, scaledWidth, scaledHeight);
            }
            System.out.println("Added: " + imagePath);
        } catch (IOException e) {
            System.err.println("Failed to process: " + imagePath);
            e.printStackTrace(System.err);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void rotate90(String src, String dest) throws Exception {
        BufferedImage bufferedImage = ImageIO.read(new File(src));
        ImageOutputStream ios = new FileImageOutputStream(new File(dest));
        ImageIO.write(rotate90(bufferedImage), FilenameUtils.getExtension(src), ios);
        ios.close();
    }

    public static BufferedImage rotate90(BufferedImage src) {
        int srcWidth = src.getWidth();
        int srcHeight = src.getHeight();
        boolean hasAlphaChannel = src.getAlphaRaster() != null;
        int pixelLength = hasAlphaChannel ? 4 : 3;
        byte[] srcPixels = ((DataBufferByte) src.getRaster().getDataBuffer()).getData();

        // Create the destination buffered image
        BufferedImage dest = new BufferedImage(srcHeight, srcWidth, src.getType());
        byte[] destPixels = ((DataBufferByte) dest.getRaster().getDataBuffer()).getData();
        int destWidth = dest.getWidth();

        int srcPos = 0; // We can just increment this since the data pack order matches our loop traversal: left to right, top to bottom. (Just like reading a book.)
        for (int srcY = 0; srcY < srcHeight; srcY++) {
            for (int srcX = 0; srcX < srcWidth; srcX++) {
                int destX = ((srcHeight - 1) - srcY);
                int destY = srcX;
                int destPos = (((destY * destWidth) + destX) * pixelLength);
                if (hasAlphaChannel) {
                    destPixels[destPos++] = srcPixels[srcPos++];    // alpha
                }
                destPixels[destPos++] = srcPixels[srcPos++];        // blue
                destPixels[destPos++] = srcPixels[srcPos++];        // green
                destPixels[destPos++] = srcPixels[srcPos++];        // red
            }
        }
        return dest;
    }

    public static void compressPdf(float factor, String src, String dest) throws IOException, DocumentException {
        PdfName key = new PdfName("ITXT_SpecialId");
        PdfName value = new PdfName("123456789");
        // Read the file
        PdfReader reader = new PdfReader(src);
        int n = reader.getXrefSize();
        PdfObject object;
        PRStream stream;
        // Look for image and manipulate image stream
        for (int i = 0; i < n; i++) {
            object = reader.getPdfObject(i);
            if (object == null || !object.isStream())
                continue;
            stream = (PRStream) object;
            // if (value.equals(stream.get(key))) {
            PdfObject pdfsubtype = stream.get(PdfName.SUBTYPE);
            System.out.println(stream.type());
            if (pdfsubtype != null && pdfsubtype.toString().equals(PdfName.IMAGE.toString())) {
                PdfImageObject image = new PdfImageObject(stream);
                BufferedImage bi = image.getBufferedImage();
                if (bi == null) continue;
                int width = (int) (bi.getWidth() * factor);
                int height = (int) (bi.getHeight() * factor);
                BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                AffineTransform at = AffineTransform.getScaleInstance(factor, factor);
                Graphics2D g = img.createGraphics();
                g.drawRenderedImage(bi, at);
                ByteArrayOutputStream imgBytes = new ByteArrayOutputStream();
                ImageIO.write(img, "JPG", imgBytes);
                stream.clear();
                stream.setData(imgBytes.toByteArray(), false, PRStream.BEST_COMPRESSION);
                stream.put(PdfName.TYPE, PdfName.XOBJECT);
                stream.put(PdfName.SUBTYPE, PdfName.IMAGE);
                stream.put(key, value);
                stream.put(PdfName.FILTER, PdfName.DCTDECODE);
                stream.put(PdfName.WIDTH, new PdfNumber(width));
                stream.put(PdfName.HEIGHT, new PdfNumber(height));
                stream.put(PdfName.BITSPERCOMPONENT, new PdfNumber(8));
                stream.put(PdfName.COLORSPACE, PdfName.DEVICERGB);
            }
        }

        // Save altered PDF
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        stamper.close();
        reader.close();
    }
}
