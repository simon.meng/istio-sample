package com.xmm.sample.servicea;

import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@Component
public class Monitor {

    private String inputDir = System.getProperty("os.name").toLowerCase().contains("windows") ? "d:/pdf" : "/tmp/pdf";
    private String outputDir = System.getProperty("os.name").toLowerCase().contains("windows") ? "d:/pdf_merged" : "/tmp/pdf_merged";


    private Map<String, Long> history = new HashMap<>();

    @PostConstruct
    public void on() {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> System.out.println("shutdown...")));
    }


    @Scheduled(initialDelay = 1000, fixedDelay = 5000)
    public void run() {
        System.out.println("开始扫描文件....");
        try {
            autoSplitPdfs();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("结束扫描文件.");
    }


    public void autoSplitPdfs() throws Exception {
        Stream<Path> steam = Files.find(Paths.get(inputDir), 1,
                (path, basicFileAttributes) -> path.toFile().getName().toLowerCase().endsWith(".pdf")
                        && (!history.containsKey(path.toFile().getPath()) ||
                        history.get(path.toFile().getPath()) != path.toFile().lastModified())
        );

        AtomicInteger counter = new AtomicInteger();
        steam.forEachOrdered(path -> {
            try {
                counter.getAndIncrement();
                System.out.println("拆分PDF: " + path.toFile().toString());
                split(path.toFile().getPath());
                history.put(path.toFile().getPath(), path.toFile().lastModified());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        if (counter.get() == 0) {
            System.out.println("没有新的PDF要拆分!");
        }

    }


    public static void split(String pdfPath) throws Exception {

        File file = new File(pdfPath);

        if (!file.exists()) {
            System.out.println("文件不存在：" + pdfPath);
            return;
        }

        if (!pdfPath.toLowerCase().endsWith(".pdf")) {
            System.out.println("文件不是PDF：" + pdfPath);
        }

        String outputDir = file.getPath().substring(0, file.getPath().length() - 4);
        new File(outputDir).mkdirs();

        PDDocument document = PDDocument.load(file);

        //Instantiating Splitter class
        Splitter splitter = new Splitter();

        //splitting the pages of a PDF document
        List<PDDocument> Pages = splitter.split(document);

        //Creating an iterator
        Iterator<PDDocument> iterator = Pages.listIterator();

        //Saving each page as an individual document
        int i = 1;
        while (iterator.hasNext()) {
            PDDocument pd = iterator.next();
            String newPdfPath = outputDir + File.separator + i++ + ".pdf";
            pd.save(newPdfPath);
            pd.close();
        }

        System.out.println(String.format("%s 成功拆分成%d个文件", pdfPath, i));
        document.close();
    }

}
