package com.xmm.sample.servicea;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageFitWidthDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;

import java.io.File;
import java.io.IOException;

public class CreateBookmarks {

    public static void main(String[] args) throws Exception{

        try (final PDDocument doc = new PDDocument()){

            PDPage page1 = new PDPage();
            PDPage page2 = new PDPage();
            PDPage page3 = new PDPage();

            doc.addPage(page1);
            doc.addPage(page2);
            doc.addPage(page3);

            PDDocumentOutline outline = new PDDocumentOutline();
            doc.getDocumentCatalog().setDocumentOutline(outline);
            PDOutlineItem pagesOutline = new PDOutlineItem();
            pagesOutline.setTitle("All Pages");
            outline.addLast(pagesOutline);

            int pageNum = 1;
            for (PDPage page : doc.getPages()) {

                // write some content
                PDPageContentStream contents = new PDPageContentStream(doc, page);
                contents.beginText();
                contents.newLineAtOffset(100, 700);
                contents.setFont(PDType1Font.HELVETICA, 12);
                contents.showText("Content page " + pageNum);
                contents.endText();
                contents.close();

                PDPageDestination dest = new PDPageFitWidthDestination();
                dest.setPage(page);
                PDOutlineItem bookmark = new PDOutlineItem();
                bookmark.setDestination(dest);
                bookmark.setTitle("Page " + pageNum);
                pagesOutline.addLast(bookmark);

                pageNum++;
            }

            pagesOutline.openNode();
            outline.openNode();

            doc.save(new File("/tmp/bookmark.pdf"));
        } catch (IOException e){
            System.err.println("Exception while trying to create pdf document - " + e);
        }
    }

}
